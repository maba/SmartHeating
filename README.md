# SmartHeating

The App can be build and debuged in any .NET 6 ready IDE for c#.
The App can be tested multiple ways. For the Calendar- and WeatherService there are Test-Classes that can be run to validate those Interfaces.
Other functionalities such as the SmartHeatService and the UI can be tested by debuging.

Über eine Wetter-API und einen Stundenplan Abgleich via Rapla soll die Heizung intelligent gesteuert werden. Dementsprechend soll sich die Heizung auf dem Rückweg von der Uni vorheizen, beziehungsweise sich beim Verlassen des Hauses abschalten.
https://miro.com/welcomeonboard/bEJOb2trUDhHemFuVmIyM0JqbzVmTG41T2ZaUHIyQ1g2d250dTEybkVwT3dJQ0tUTVV2ZlFkdlNPc0Nma3VsZHwzMDc0NDU3MzY2NTg3NzU0MzI2?invite_link_id=592670402400