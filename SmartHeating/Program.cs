using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using SmartHeating;
using SmartHeating.Services.Health;
using SmartHeating.Services.Health.Classes;
using SmartHeating.Services.SmartHeat;
using SmartHeating.Services.SmartHeat.Classes;

public class Program
{
    /// <summary>
    /// Main-Method of this Application. Initializes SmartHeatService and HealthService.
    /// </summary>
    /// <param name="args"></param>
    public static async Task Main(string[] args)
    {
        var builder = WebAssemblyHostBuilder.CreateDefault(args);
        builder.RootComponents.Add<App>("#app");
        builder.RootComponents.Add<HeadOutlet>("head::after");

        builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
        builder.Services.AddSingleton<ISmartHeatService, SmartHeatService>();
        builder.Services.AddSingleton<IHealthService, HealthService>();
        await builder.Build().RunAsync();
    }
}


