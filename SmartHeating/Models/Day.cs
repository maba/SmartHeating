﻿using SmartHeating.Services.Calendar.Classes;
using SmartHeating.Services.Weather;

namespace SmartHeating.Models
{
    public class Day
    {
        public DateTime Date { get;set; }
        public List<CalendarEvent> CalendarEvents { get; set; }
        public List<WeatherData> WeatherData { get; set; }
        public DateTime HeatOff { get; set; }
        public DateTime HeatOn { get; set; }
    }
}
