﻿namespace SmartHeating.Models
{
    public class HeatSettings
    {
        public int ArrivalTime { get; set; } = 50;
        public int HeatTime { get; set; } = 30;
        public string RaplaUrl { get; set; } = "https://rapla.dhbw-karlsruhe.de/rapla?page=calendar&user=strand&file=TINF20B5";
        public string CityName { get; set; } = "Karlsruhe";
        public int Temperature { get; set; } = 22;
    }
}
