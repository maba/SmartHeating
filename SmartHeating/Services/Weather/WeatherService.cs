﻿using Json.Net;
using SmartHeating.Services.Weather.Classes;

namespace SmartHeating.Services.Weather;

/// <summary>
/// WeatherService connects the Weather-API with the SmartHeatService
/// </summary>
public class WeatherService : IWeatherService
{
    /// <summary>
    /// Locks the creation of an instance
    /// </summary>
    private static readonly object _lock = new();

    /// <summary>
    /// The created instance of the first call
    /// </summary>
    private static WeatherService? _instance;

    /// <summary>
    /// WeatherServiceConnector for API-Calls
    /// </summary>
    private readonly IWeatherServiceConnector _connector = WeatherServiceConnector.Instance;

    /// <summary>
    /// Gets or creates an instance of WeatherService
    /// </summary>
    public static WeatherService Instance
    {
        get
        {
            lock (_lock)
            {
                return _instance ??= new WeatherService();
            }
        }
    }

    /// <summary>
    /// Gets locations specified by an input string, containing a city/village name
    /// </summary>
    /// <param name="input">string input</param>
    /// <returns> List of LocationData with latitude, longitude, name, state, country </returns>
    public async Task<List<LocationData>> GetLocations(string input)
    {
        var jsonString = await _connector.GetLocationsJson(input);
        var locations = JsonNet.Deserialize<dynamic>(jsonString);

        var ret = new List<LocationData>();
        foreach (var location in locations)
        {
            var locationData = new LocationData
            {
                Name = location.name,
                State = location.state,
                Country = location.country,
                Latitude = location.lat,
                Longitude = location.lon
            };
            ret.Add(locationData);
        }

        return ret;
    }

    /// <summary>
    /// Gets the current weather at coordinates.
    /// </summary>
    /// <param name="lat">latitude</param>
    /// <param name="lon">longitude</param>
    /// <returns> WeatherData object </returns>
    public async Task<WeatherData> GetCurrentWeather(double lat, double lon)
    {
        var jsonString = await _connector.GetCurrentWeatherJson(lat, lon);
        var weather = JsonNet.Deserialize<dynamic>(jsonString);

        var ret = new WeatherData
        {
            Timestamp = DateTimeOffset.FromUnixTimeSeconds((long)weather.dt),
            Temperature = (int)((weather.main.temp - 273.15) * 10),
            WeatherType = WeatherTypeHelper.FromCode(weather.weather[0].icon)
        };
        return ret;
    }

    /// <summary>
    /// Gets the Weather Forecast at coordinates for the 5 days (every 3h).
    /// </summary>
    /// <param name="lat">latitude</param>
    /// <param name="lon">longitude</param>
    /// <returns> List of WeatherData objects </returns>
    public async Task<List<WeatherData>> GetForecastWeather(double lat, double lon)
    {
        var jsonString = await _connector.GetForecastWeatherJson(lat, lon);
        var forecasts = JsonNet.Deserialize<dynamic>(jsonString);

        var ret = new List<WeatherData>();
        foreach (var forecast in forecasts.list)
        {
            var weatherData = new WeatherData
            {
                Timestamp = DateTimeOffset.FromUnixTimeSeconds((long)forecast.dt),
                Temperature = (int)((forecast.main.temp - 273.15) * 10),
                WeatherType = WeatherTypeHelper.FromCode(forecast.weather[0].icon)
            };
            ret.Add(weatherData);
        }

        return ret;
    }
}


