﻿namespace SmartHeating.Services.Weather.Classes
{
    /// <summary>
    /// Interface for WeatherService
    /// </summary>
    public interface IWeatherService
    {
        public Task<List<LocationData>> GetLocations(string input);
        public Task<WeatherData> GetCurrentWeather(double lat, double lon);
        public Task<List<WeatherData>> GetForecastWeather(double lat, double lon);
    }
}
