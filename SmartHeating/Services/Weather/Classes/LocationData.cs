﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// LocationData Model
    /// </summary>
    public class LocationData
    {
        /// <summary>
        /// Location Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public double Longitude { get; set; }
    }
}
