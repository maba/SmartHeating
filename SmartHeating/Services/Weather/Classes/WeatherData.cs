﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// WeatherData Model
    /// </summary>
    public class WeatherData
    {
        /// <summary>
        /// Timestamp of Weather-Data
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// Temperature in C
        /// </summary>
        public int Temperature { get; set; }

        /// <summary>
        /// WeatherType Enum
        /// </summary>
        public WeatherType WeatherType { get; set; }
    }
}
