﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// Decoder for the WeatherType from the API
    /// </summary>
    public static class WeatherTypeHelper
    {
        /// <summary>
        /// Decodes API-Response-Type to Enum
        /// </summary>
        /// <param name="code">code</param>
        /// <returns> WeatherType-Enum </returns>
        public static WeatherType FromCode(string code)
        {
            return code switch
            {
                "01d" => WeatherType.ClearDay,
                "01n" => WeatherType.ClearNight,
                "02d" => WeatherType.FewCloudsDay,
                "02n" => WeatherType.FewCloudsNight,
                "03d" => WeatherType.ScatteredCloudsDay,
                "03n" => WeatherType.ScatteredCloudsNight,
                "04d" => WeatherType.BrokenCloudsDay,
                "04n" => WeatherType.BrokenCloudsNight,
                "09d" => WeatherType.ShowerRainDay,
                "09n" => WeatherType.ShowerRainNight,
                "10d" => WeatherType.RainDay,
                "10n" => WeatherType.RainNight,
                "11d" => WeatherType.ThunderstormDay,
                "11n" => WeatherType.ThunderstormNight,
                "13d" => WeatherType.SnowDay,
                "13n" => WeatherType.SnowNight,
                "50d" => WeatherType.MistDay,
                "50n" => WeatherType.MistNight,
                _ => WeatherType.Unknown
            };
        }
    }
}
