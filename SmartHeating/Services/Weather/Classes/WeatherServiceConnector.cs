﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// WeatherServiceConnector handles Weather-API-Requests
    /// </summary>
    internal class WeatherServiceConnector : IWeatherServiceConnector
    {
        /// <summary>
        /// Locks the creation of an instance
        /// </summary>
        private static readonly object _lock = new();

        /// <summary>
        /// The created instance of the first call
        /// </summary
        private static WeatherServiceConnector? _instance;

        /// <summary>
        /// Base-URL of the Weather-API
        /// </summary>
        private const string WeatherApiUrl = "https://api.openweathermap.org";

        /// <summary>
        /// Gets or creates an instance of WeatherServiceConnector
        /// </summary>
        public static WeatherServiceConnector Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance ??= new WeatherServiceConnector();
                }
            }
        }

        /// <summary>
        /// API-Call for Locations
        /// </summary>
        /// <param name="input">input string, locationname</param>
        /// <returns>json as string</returns>
        public async Task<string> GetLocationsJson(string input)
        {
            return await Fetch($"{WeatherApiUrl}/geo/1.0/direct?appid={GetApiKey()}&q={input}");
        }

        /// <summary>
        /// API-Call for current Weather at Location
        /// </summary>
        /// <param name="lat">latitude</param>
        /// <param name="lon">longitude</param>
        /// <returns>json as string</returns>
        public async Task<string> GetCurrentWeatherJson(double lat, double lon)
        {
            return await Fetch($"{WeatherApiUrl}/data/2.5/weather?appid={GetApiKey()}&lat={lat}&lon={lon}");
        }

        /// <summary>
        /// API-Call for weather forecast
        /// </summary>
        /// <param name="lat">latitude</param>
        /// <param name="lon">longitude</param>
        /// <returns>json as string</returns>
        public async Task<string> GetForecastWeatherJson(double lat, double lon)
        {
            return await Fetch($"{WeatherApiUrl}/data/2.5/forecast?appid={GetApiKey()}&lat={lat}&lon={lon}");
        }

        /// <summary>
        /// Executes HTTP Request
        /// </summary>
        /// <param name="url">Specified API-URL</param>
        /// <returns>json as string</returns>
        private async Task<string> Fetch(string url)
        {
            var client = new HttpClient();
            return await client.GetStringAsync(url).ConfigureAwait(false);
        }

        /// <summary>
        /// API-key for URL
        /// </summary>
        /// <returns>API-Key as string</returns>
        private async Task<string> GetApiKey()
        {
            return "9c2e3f301e6b5e69e1d5f37b0260297e";
        }
    }
}
