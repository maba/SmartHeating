﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// Interface for WeatherServiceConnector
    /// </summary>
    public interface IWeatherServiceConnector
    {
        public Task<string> GetLocationsJson(string input);
        public Task<string> GetCurrentWeatherJson(double lat, double lon);
        public Task<string> GetForecastWeatherJson(double lat, double lon);
    }
}
