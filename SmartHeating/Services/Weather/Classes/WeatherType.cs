﻿namespace SmartHeating.Services.Weather
{
    /// <summary>
    /// Enum for weather type
    /// </summary>
    public enum WeatherType
    {
        ClearDay, ClearNight, FewCloudsDay, FewCloudsNight, ScatteredCloudsDay, ScatteredCloudsNight,
        BrokenCloudsDay, BrokenCloudsNight, ShowerRainDay, ShowerRainNight, RainDay, RainNight,
        ThunderstormDay, ThunderstormNight, SnowDay, SnowNight, MistDay, MistNight, Unknown
    }
}
