﻿namespace SmartHeating.Services.Health.Classes
{
    /// <summary>
    /// Interface for HealthService
    /// </summary>
    public interface IHealthService
    {
        public Task WriteNewHealthIssue(Exception message);
    }
}
