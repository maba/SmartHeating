﻿using SmartHeating.Services.Health.Classes;

namespace SmartHeating.Services.Health
{
    /// <summary>
    /// HealthService for Error collection
    /// </summary>
    public class HealthService : IHealthService
    {
        /// <summary>
        /// Path of ErrorFile
        /// </summary>
        private const string errorFilePath = "\\errorfile.txt";

        /// <summary>
        /// Writes Health Issue in Error-File
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>Task</returns>
        public async Task WriteNewHealthIssue(Exception exception)
        {
            await File.AppendAllLinesAsync(errorFilePath, ErrorMessageBuilder(exception));
        }

        /// <summary>
        /// Builds an Error Message from Exception
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>List of strings</returns>
        public List<string> ErrorMessageBuilder(Exception exception)
        {
            var message = new List<string>() { "NEW ERROR", exception.Message, exception.InnerException.Message, exception.InnerException.StackTrace, "" };

            return message;
        }
    }
}
