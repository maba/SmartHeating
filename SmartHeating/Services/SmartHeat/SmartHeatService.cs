﻿using SmartHeating.Services.Calendar;
using SmartHeating.Services.Calendar.Classes;
using SmartHeating.Services.Health.Classes;
using SmartHeating.Services.Heating;
using SmartHeating.Services.Heating.Classes;
using SmartHeating.Services.SmartHeat.Classes;
using SmartHeating.Services.Weather;
using SmartHeating.Services.Weather.Classes;

namespace SmartHeating.Services.SmartHeat
{
    /// <summary>
    /// SmartHeatService binds all services. Collects Data, sends message to schedule HeatJobs, comminicates with HealthService.
    /// </summary>
    public class SmartHeatService : ISmartHeatService
    {
        /// <summary>
        /// User Settings for Smart Heating
        /// </summary>
        public HeatSettings Settings { get; set; }

        /// <summary>
        /// List of Days containing weather forecast, appointments, heat on/off time and date.
        /// </summary>
        public List<Day> Days { get; set; } = new();

        /// <summary>
        /// Health Service Instance
        /// </summary>
        private IHealthService _healthService;

        /// <summary>
        /// Weather Service Instance
        /// </summary>
        private IWeatherService _weatherService;

        /// <summary>
        /// Calendar Service Instance
        /// </summary>
        private ICalendarService _calendarService;

        /// <summary>
        /// Heating Service Instance
        /// </summary>
        private IHeatingService _heatingService;

        /// <summary>
        /// Constructor gets Service-Instances and Settings
        /// </summary>
        /// <param name="healthService">HealthService injected</param>
        public SmartHeatService(IHealthService healthService)
        {
            _healthService = healthService;

            _weatherService = WeatherService.Instance;

            _calendarService = CalendarService.Instance;

            _heatingService = HeatingService.Instance;

            Settings = new HeatSettings();
        }

        /// <summary>
        /// Fetches Calendar- and Weather-Data, logs heating jobs.
        /// </summary>
        /// <returns>Task</returns>
        public async Task InitializeService()
        {
            var weatherForecast = await GetWeather();

            Days = await MapForecastToEvents(weatherForecast);

            Days.Select(_heatingService.AddEventToSchedule);
        }

        /// <summary>
        /// Maps Forecast to Events
        /// </summary>
        /// <param name="forecast">forecast</param>
        /// <returns>List of Days</returns>
        private async Task<List<Day>> MapForecastToEvents(List<WeatherData> forecast)
        {
            var days = new List<Day>();

            var date = DateTime.Today;
            var nextDate = date.AddDays(1);

            try
            {
                while (true)
                {
                    var forecastOfDay = forecast.Where(x => x.Timestamp > date && x.Timestamp < nextDate).OrderBy(x => x.Timestamp).ToList();

                    if (!forecastOfDay.Any()) break;
                    var eventsOfDay = await _calendarService.GetCalendarEventsForDay(Settings.RaplaUrl, date);

                    DateTime heatOff = DateTime.Today;
                    DateTime heatOn = DateTime.Today;

                    if (eventsOfDay.Any())
                    {
                        heatOff = eventsOfDay.First().Begin.AddMinutes(0 - Settings.ArrivalTime);
                        heatOn = eventsOfDay.Last().End.AddMinutes(Settings.ArrivalTime).AddMinutes(0 - Settings.HeatTime);
                    }

                    days.Add(new Day
                    {
                        Date = date,
                        WeatherData = forecastOfDay,
                        CalendarEvents = eventsOfDay,
                        HeatOff = heatOff,
                        HeatOn = heatOn
                    });

                    date = date.AddDays(1);
                    nextDate = nextDate.AddDays(1);
                }
            }
            catch (Exception e)
            {
                var newException = new Exception("Failed mapping Weather-Data to Events", e);
                await _healthService.WriteNewHealthIssue(newException);
                throw newException;
            }

            return days;
        }

        /// <summary>
        /// Gets Weather Forecast from WeatherService
        /// </summary>
        /// <returns>List of WeatherData</returns>
        private async Task<List<WeatherData>> GetWeather()
        {
            try
            {
                var locations = await _weatherService.GetLocations(Settings.CityName);
                var location = locations.First();

                return await _weatherService.GetForecastWeather(location.Latitude, location.Longitude);
            }
            catch (Exception e)
            {
                var newException = new Exception("Failed accessing Weather-Data.", e);
                await _healthService.WriteNewHealthIssue(newException);
                throw newException;
            }
        }

        /// <summary>
        /// Saves new Settings, called by UI
        /// </summary>
        /// <param name="settings">new Settings</param>
        public async void SaveSettings(HeatSettings settings)
        {
            if (Settings.CityName != settings.CityName
                || Settings.RaplaUrl != settings.RaplaUrl
                || Settings.HeatTime != settings.HeatTime
                || Settings.ArrivalTime != settings.ArrivalTime)
            {
                Settings = settings;
                _heatingService.HeatingEvents = null;
                await InitializeService();
                return;
            }
            if (Settings.Temperature != settings.Temperature)
            {
                Settings = settings;
                _heatingService.UpdateTemperature(settings.Temperature);
            }
        }
    }
}
