﻿namespace SmartHeating.Services.SmartHeat.Classes
{
    /// <summary>
    /// Interface for SmartHeatService
    /// </summary>
    public interface ISmartHeatService
    {
        public HeatSettings Settings { get; set; }
        public List<Day> Days { get; set; }
        public Task InitializeService();
        public void SaveSettings(HeatSettings settings);
    }
}
