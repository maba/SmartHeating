﻿using SmartHeating.Services.Calendar.Classes;
using SmartHeating.Services.Weather;

namespace SmartHeating.Services.SmartHeat.Classes
{
    /// <summary>
    /// Modle for Day
    /// </summary>
    public class Day
    {
        /// <summary>
        /// Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// List of all Events
        /// </summary>
        public List<CalendarEvent> CalendarEvents { get; set; }

        /// <summary>
        /// List of all of the days weather data
        /// </summary>
        public List<WeatherData> WeatherData { get; set; }

        /// <summary>
        /// Time for heat off
        /// </summary>
        public DateTime HeatOff { get; set; }

        /// <summary>
        /// time for heat on
        /// </summary>
        public DateTime HeatOn { get; set; }
    }
}
