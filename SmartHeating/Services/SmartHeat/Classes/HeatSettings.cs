﻿namespace SmartHeating.Services.SmartHeat.Classes
{
    /// <summary>
    /// User specific settings with base values
    /// </summary>
    public class HeatSettings
    {
        /// <summary>
        /// Time you need from Uni to Home
        /// </summary>
        public int ArrivalTime { get; set; } = 50;

        /// <summary>
        /// Time ur flat/house needs to heat up
        /// </summary>
        public int HeatTime { get; set; } = 30;

        /// <summary>
        /// Rapla URL
        /// </summary>
        public string RaplaUrl { get; set; } = "https://rapla.dhbw-karlsruhe.de/rapla?page=calendar&user=strand&file=TINF20B5";
        
        /// <summary>
        /// City Name
        /// </summary>
        public string CityName { get; set; } = "Karlsruhe";

        /// <summary>
        /// Temperature
        /// </summary>
        public int Temperature { get; set; } = 20;
    }
}
