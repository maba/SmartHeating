﻿using SmartHeating.Services.Heating.Classes;
using SmartHeating.Services.SmartHeat.Classes;

namespace SmartHeating.Services.Heating
{
    /// <summary>
    /// HeatingService for heating jobs
    /// </summary>
    public class HeatingService : IHeatingService
    {
        /// <summary>
        /// Locks the creation of an instance
        /// </summary>
        private static readonly object _lock = new();

        /// <summary>
        /// The created instance of the first call
        /// </summary>
        private static HeatingService? _instance;

        /// <summary>
        /// Users entered Temperature
        /// </summary>
        public int Temperature { get; set; }

        /// <summary>
        /// List of running HeatingEvents
        /// </summary>
        public List<HeatingEvent> HeatingEvents { get; set; }

        /// <summary>
        /// Gets or creates an instance of HeatingService
        /// </summary>
        public static HeatingService Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance ??= new HeatingService();
                }
            }
        }

        /// <summary>
        /// Adds an Event to the List of HeatingEvents
        /// </summary>
        /// <param name="day">Day-object</param>
        /// <returns>Task</returns>
        public async Task AddEventToSchedule(Day day)
        {
            try
            {
                var heatingEvent = new HeatingEvent(day, Temperature);
                HeatingEvents.Add(heatingEvent);
                HeatingEventWatchdog(heatingEvent);
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// waiting for the completion of the heating event to delete it from List
        /// </summary>
        /// <param name="heatingEvent">the Event</param>
        /// <returns>Task</returns>
        private async Task HeatingEventWatchdog(HeatingEvent heatingEvent)
        {
            await heatingEvent.HeatOffTimer;
            await heatingEvent.HeatOnTimer;

            HeatingEvents.Remove(heatingEvent);
        }

        /// <summary>
        /// Updates the Temperature from Settings
        /// </summary>
        /// <param name="temperature">new temperature</param>
        public void UpdateTemperature(int temperature)
        {
            foreach (var heatingEvent in HeatingEvents)
            {
                HeatingEvents[HeatingEvents.IndexOf(heatingEvent)].Temperature = temperature;
            }
        }
    }
}
