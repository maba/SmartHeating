﻿using SmartHeating.Services.SmartHeat.Classes;

namespace SmartHeating.Services.Heating.Classes
{
    /// <summary>
    /// HeatingEvent Model
    /// </summary>
    public class HeatingEvent
    {
        /// <summary>
        /// Timer-Task for HeatOn
        /// </summary>
        public Task HeatOnTimer { get; }

        /// <summary>
        /// Timer-Task for HeatOff
        /// </summary>
        public Task HeatOffTimer { get; }

        /// <summary>
        /// Temperature from Settings
        /// </summary>
        public int Temperature { get; set; }

        /// <summary>
        /// Starts Heating Job for a day
        /// </summary>
        /// <param name="day">Day-Object</param>
        /// <param name="temperature">temperature</param>
        public HeatingEvent(Day day, int temperature)
        {
            var timeTillHeatOff = day.HeatOff - DateTime.Now;
            HeatOffTimer = Task.Delay(timeTillHeatOff.Milliseconds);
            StartWatchdog(HeatOffTimer, HeatOff);

            var timeTillHeatOn = day.HeatOn - DateTime.Now;
            HeatOnTimer = Task.Delay(timeTillHeatOn.Milliseconds);
            StartWatchdog(HeatOnTimer, HeatOn);

            Temperature = temperature;
        }

        /// <summary>
        /// Starts Watchdog for Heating Task
        /// </summary>
        /// <param name="timer">Timer-Task</param>
        /// <param name="task">Function, HeatOn()/HeatOff()</param>
        /// <returns></returns>
        private async Task StartWatchdog(Task timer, Func<Task> task)
        {
            await timer;
            await task();
        }

        /// <summary>
        /// Turns on Heating
        /// </summary>
        /// <returns>Task</returns>
        private async Task HeatOn()
        {
            //placeholder for turning heat up to specified temperature
        }

        /// <summary>
        /// Turns down Heating
        /// </summary>
        /// <returns>Task</returns>
        private async Task HeatOff()
        {
            //placeholder for stop heating
        }
    }
}
