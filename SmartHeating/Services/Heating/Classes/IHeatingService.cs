﻿using SmartHeating.Services.SmartHeat.Classes;

namespace SmartHeating.Services.Heating.Classes
{
    /// <summary>
    /// Interface for HeatingService
    /// </summary>
    public interface IHeatingService
    {
        public int Temperature { get; set; }
        public List<HeatingEvent> HeatingEvents { get; set; }
        public Task AddEventToSchedule(Day day);
        public void UpdateTemperature(int temperature);
    }
}
