﻿using Ical.Net.DataTypes;
using SmartHeating.Services.Calendar.Classes;

namespace SmartHeating.Services.Calendar
{
    /// <summary>
    /// CalendarService connects Callendar with SmartHeatService
    /// </summary>
    public class CalendarService : ICalendarService
    {
        /// <summary>
        /// Locks the creation of an instance
        /// </summary>
        private static readonly object _lock = new();

        /// <summary>
        /// The created instance of the first call
        /// </summary>
        private static CalendarService? _instance;

        /// <summary>
        /// CalendarServiceConnector for reading Calendar
        /// </summary>
        private readonly ICalendarServiceConnector _connector = CalendarServiceConnector.Instance;

        /// <summary>
        /// Gets or creates an instance of WeatherService
        /// </summary>
        public static CalendarService Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance ??= new CalendarService();
                }
            }
        }

        /// <summary>
        /// Gets all Events from Calendar
        /// </summary>
        /// <param name="url">Rapla URL</param>
        /// <returns>List of Calendar Events</returns>
        public async Task<List<CalendarEvent>> GetAllCalendarEvents(string url)
        {
            var rawData = await _connector.GetRawCalendarData(url);
            var events = Ical.Net.Calendar.Load(rawData).Events;

            return IcalCalendarEventToCalendarEvent(events.ToList());
        }

        /// <summary>
        /// Gets CalendarEvents for specified Day
        /// </summary>
        /// <param name="url">Rapla URL</param>
        /// <param name="day">Date</param>
        /// <returns></returns>
        public async Task<List<CalendarEvent>> GetCalendarEventsForDay(string url, DateTime day) =>
            (await GetAllCalendarEvents(url)).FindAll(calEv => calEv.Begin.Date.CompareTo(day.Date) == 0).ToList();

        /// <summary>
        /// maps iCalCalendarEvents to CalendarEvents
        /// </summary>
        /// <param name="events">original event</param>
        /// <returns>new event</returns>
        private static List<CalendarEvent> IcalCalendarEventToCalendarEvent(List<Ical.Net.CalendarComponents.CalendarEvent> events) =>
            events.ConvertAll(calendarEvent => new CalendarEvent()
            {
                Begin = new CalDateTime(calendarEvent.Start).Value,
                End = new CalDateTime(calendarEvent.End).Value,
                Name = calendarEvent.Summary,
                Uid = calendarEvent.Uid
            }).ToList();
    }
}