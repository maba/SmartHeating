﻿namespace SmartHeating.Services.Calendar.Classes
{
    /// <summary>
    /// Interface for CalendarServiceConnector
    /// </summary>
    public interface ICalendarServiceConnector
    {
        public Task<string> GetRawCalendarData(string url);
    }
}

