﻿namespace SmartHeating.Services.Calendar.Classes
{
    /// <summary>
    /// Model for CalendarEvent
    /// </summary>
    public class CalendarEvent
    {
        /// <summary>
        /// Event ID
        /// </summary>
        public string? Uid { get; set; }

        /// <summary>
        /// Event Name
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Event Begin
        /// </summary>
        public DateTime Begin { get; set; }

        /// <summary>
        /// Event End
        /// </summary>
        public DateTime End { get; set; }
    }
}