﻿namespace SmartHeating.Services.Calendar.Classes
{
    /// <summary>
    /// Interface for CalendarService
    /// </summary>
    public interface ICalendarService
    {
        public Task<List<CalendarEvent>> GetAllCalendarEvents(string url);
        public Task<List<CalendarEvent>> GetCalendarEventsForDay(string url, DateTime day);
    }
}