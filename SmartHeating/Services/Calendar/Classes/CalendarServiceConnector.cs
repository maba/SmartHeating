﻿namespace SmartHeating.Services.Calendar.Classes
{
    /// <summary>
    /// CalendarServiceConnector handles the Calendar-Requests
    /// </summary>
    public class CalendarServiceConnector : ICalendarServiceConnector
    {
        /// <summary>
        /// Locks the creation of an instance
        /// </summary>
        private static readonly object _lock = new();

        /// <summary>
        /// The created instance of the first call
        /// </summary
        private static CalendarServiceConnector? _instance;

        /// <summary>
        /// Gets or creates an instance of CalendarServiceConnector
        /// </summary>
        public static CalendarServiceConnector Instance
        {
            get
            {
                lock (_lock)
                {
                    return _instance ??= new CalendarServiceConnector();
                }
            }
        }

        /// <summary>
        /// Gets Calendar-Data as ical-string
        /// </summary>
        /// <param name="url">Rapla URL</param>
        /// <returns>json string</returns>
        public async Task<string> GetRawCalendarData(string url)
        {
            return await Fetch(url);
        }

        /// <summary>
        /// HTTP-Request for URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static async Task<string> Fetch(string url)
        {
            return await new HttpClient().GetStringAsync(url);
        }
    }
}