﻿using Microsoft.AspNetCore.Components;
using SmartHeating.Services.SmartHeat.Classes;

namespace SmartHeating.Pages
{
    /// <summary>
    /// Code-Behind of Settings Page
    /// </summary>
    [Route($"settings")]
    public partial class Settings : ComponentBase
    {
        /// <summary>
        /// Injected SmartHeatService
        /// </summary>
        [Inject]
        public ISmartHeatService SmartHeatService { get; set; }

        /// <summary>
        /// settings
        /// </summary>
        private int _arrivalTime;
        private int _heatTime;
        private string _raplaUrl;
        private string _cityName;
        private int _temperature;

        /// <summary>
        /// Initialization of Data gets called before page loads
        /// </summary>
        /// <returns></returns>
        protected override async Task OnInitializedAsync()
        {
            _arrivalTime = SmartHeatService.Settings.ArrivalTime;
            _heatTime = SmartHeatService.Settings.HeatTime;
            _raplaUrl = SmartHeatService.Settings.RaplaUrl;
            _cityName = SmartHeatService.Settings.CityName;
            _temperature = SmartHeatService.Settings.Temperature;
        }

        /// <summary>
        /// Saves Settings on Buttonclick
        /// </summary>
        private void SaveSettings()
        {
            var settings = new HeatSettings
            {
                HeatTime = _heatTime,
                ArrivalTime = _arrivalTime,
                RaplaUrl = _raplaUrl,
                CityName = _cityName,
                Temperature = _temperature
            };

            SmartHeatService.SaveSettings(settings);
        }
    }
}
