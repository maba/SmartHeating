﻿using Microsoft.AspNetCore.Components;
using SmartHeating.Services.SmartHeat.Classes;

namespace SmartHeating.Pages
{
    /// <summary>
    /// Code-Behind of Calendar Page
    /// </summary>
    [Route($"/calendar")]
    public partial class Calendar : ComponentBase
    {
        /// <summary>
        /// Injected SmartHeatService
        /// </summary>
        [Inject]
        public ISmartHeatService SmartHeatService { get; set; }

        /// <summary>
        /// List of Days
        /// </summary>
        List<Day> Days { get; set; }

        /// <summary>
        /// Error message and flag
        /// </summary>
        public bool ErrorReadingSchedule = false;
        public string ErrorMessage = string.Empty;

        /// <summary>
        /// Initialization of Data gets called before page loads
        /// </summary>
        /// <returns></returns>
        protected override async Task OnInitializedAsync()
        {
            try
            {
                await SmartHeatService.InitializeService();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                ErrorReadingSchedule = true;
            }

            Days = SmartHeatService.Days;
        }
    }
}
